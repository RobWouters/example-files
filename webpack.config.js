const path = require('path');
const webpack = require('webpack')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const fs = require('fs')

const HERE = path.resolve(__dirname)
const SRC = path.join(HERE, 'src')
const STATIC = path.join(HERE, 'backend', 'static', 'compiled')
const DEV = process.env.NODE_ENV !== 'production'

const extractSASS = new ExtractTextPlugin({
    filename: "[name].css",
    disable: DEV,
})

module.exports = {
    entry: {
        login: './src/login/main.ts',
        main: './src/main.ts',
        vendor: [
            'jquery',
            'angular',
            'angular-ui-router',
            'restangular',
        ],
    },
    output: {
        path: STATIC,
        filename: '[name].js',
        publicPath: '/s/compiled/',
    },
    resolve: {
        extensions: ['.ts', '.js'],
    },
    module: {
        rules: [{
            test: /\.ts$/,
            use: ['awesome-typescript-loader'],
        }, {
            test: /\.html$/,
            use: ['ngtemplate-loader?relativeTo=' + SRC, {
                loader: 'html-loader',
                options: {
                    minimize: true,
                },
            }],
        }, {
            test: /\.(scss|sass)$/,
            use: extractSASS.extract({
                use: [{
                    loader: 'css-loader',
                    options: {
                        minimize: true,
                    },
                }, {
                    loader: 'sass-loader',
                }],
                fallback: 'style-loader', 
            }),
        }, {
            test: /\.css$/,
            use: ['style-loader', 'css-loader'],
        }, {
            test: /\.(woff2?|ttf|eot|svg)(\?.+)?$/,
            use: ['file-loader'],
        }, {
            test: /\.(jpg|png|gif)$/,
            use: ['file-loader'],
        }],
    },
    plugins: [
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery',
        }),
        new webpack.optimize.CommonsChunkPlugin({name: 'vendor', filename: 'vendor.js'}),
        extractSASS,
    ],
}
if (DEV) {
    const PORT = parseInt(fs.readFileSync('port', 'utf8').trim())
    const DEV_PORT = PORT + 1
    module.exports.devServer = {
        port: DEV_PORT,
        host: '0.0.0.0',
        proxy: {
            '/': `http://localhost:${PORT}`,
        },
        publicPath: module.exports.output.publicPath,
    }
} else {
    module.exports.plugins.push(new webpack.optimize.UglifyJsPlugin({
        mangle: false,
    }))
}
