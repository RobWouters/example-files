#!/usr/bin/env node

// Usage:
//      cat input.html | ./chrome-pdf.js output.pdf [--landscape]

const pdf = require('html-pdf-chrome')

const options = {
    landscape: process.argv[3] === '--landscape',
    printBackground: true,
    marginLeft: .3,
    marginRight: .3,
    marginTop: .35,
    marginBottom: .7,
    paperWidth: 8.27,
    paperHeight: 11.7,
    scale: .25,
    headerTemplate: `<div>header</div>`,
    footerTemplate: `<div>footer</div>`,
    displayHeaderFooter: true,
}

let html = ''
process.stdin.on('readable', () => {
    const chunk = process.stdin.read()
    if (chunk !== null) {
        html += chunk
    }
})
process.stdin.on('end', () => {
    const p = pdf.create(html, {printOptions: options, port: 9222})
    p.then(pdf => {
        pdf.toFile(process.argv[2])
    }, err => {
        console.log(err.message)
        process.exit(1)
    })
})
